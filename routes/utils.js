const {
  getSandbox_0,
  getSandbox_1,
  getSandbox_2,
  getSandbox_3,
} = require('./sandboxes');

const getSandbox = (task, code) => {
  switch (task) {
    case 0:
      return getSandbox_0(code);
    case 1:
      return getSandbox_1(code);
    case 2:
      return getSandbox_2(code);
    case 3:
      return getSandbox_3(code);
    default:
      return {};
  }
};

const getOptions = (task, code) => ({
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
  body: JSON.stringify(getSandbox(task, code)),
});

module.exports = { getOptions };
