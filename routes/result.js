const { getOptions } = require('./utils');

const express = require('express');
const router = express.Router();

const fetch = require('node-fetch');

const fetchId = async (task, code) => {
  const res = await fetch(
    'https://codesandbox.io/api/v1/sandboxes/define?json=1',
    getOptions(task, code),
  );

  const id = await res.json();
  return id?.sandbox_id || 0;
};

const puppeteer = require('puppeteer');

const testCode = (task, code) =>
  new Promise(async resolve => {
    const id = await fetchId(task, code);
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(`https://codesandbox.io/embed/${id}?previewwindow=tests`);
    console.log(`https://codesandbox.io/embed/${id}?previewwindow=tests`);

    const interval = setInterval(async () => {
      const passedTests = await page.$$("div[class*='elements__PassedTests']");
      const failedTests = await page.$$("div[class*='elements__FailedTests']");
      page.screenshot({ path: 'screenshot.png' });

      if (!!passedTests.length || !!failedTests.length) {
        clearInterval(interval);
        resolve(!failedTests.length);
      }
    }, 1000);

    setTimeout(() => {
      console.log('dead');
      clearInterval(interval);
      resolve(false);
    }, 30000);
  });

const getLink = async (task, code) => {
  const id = await fetchId(task, code);
  return `https://codesandbox.io/embed/${id}?hidenavigation=1&hidedevtools=1&editorsize=0`;
};

router.post('/tests', async function (req, res, next) {
  try {
    const { id, code } = req.body;
    const testRes = await testCode(id, code);
    res.status(200).send({ testRes });
  } catch {
    res.status(500);
  }
});

router.post('/link', async function (req, res, next) {
  try {
    const { id, code } = req.body;
    const link = await getLink(id, code);
    res.status(200).send({ link });
  } catch {
    res.status(500);
  }
});

module.exports = router;
