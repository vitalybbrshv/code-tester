const defaultPackageJson = {
  content: {
    dependencies: {
      enzyme: '3.11.0',
      'enzyme-adapter-react-16': '1.15.6',
      react: '16.14.0',
      'react-dom': '16.14.0',
      'react-scripts': '4.0.3',
    },
  },
};

const defaultIndexJs = {
  content:
    'import React, { StrictMode } from "react";\n' +
    'import ReactDOM from "react-dom";\n' +
    '\n' +
    'import App from "./App";\n' +
    '\n' +
    'const rootElement = document.getElementById("root");\n' +
    'ReactDOM.render(\n' +
    '  <StrictMode>\n' +
    '    <App />\n' +
    '  </StrictMode>,\n' +
    '  rootElement\n' +
    ');\n',
};

//TEST
const getSandbox_0 = code => ({
  files: {
    'src/index.js': defaultIndexJs,
    'src/App.js': {
      content:
        'import React from "react";\n' +
        '\n' +
        code +
        '\n' +
        'export default function App() {\n' +
        '  return (\n' +
        '    <div className="App">\n' +
        `fn(2) = {fn(2)}` +
        '<br/>' +
        `fn(4) = {fn(4)}` +
        '<br/>' +
        `fn(6) = {fn(6)}` +
        '    </div>\n' +
        '  );\n' +
        '}\n',
    },
    'src/index.test.js': {
      content:
        "import {fn} from './App.js'\n" +
        "    it('should do smth right', () => {\n" +
        '        expect(fn(2)).toEqual(4);\n' +
        '        expect(fn(4)).toEqual(6);\n' +
        '    });\n',
    },
    'package.json': defaultPackageJson,
  },
});

// создать простейшую форму с логином и паролем и кнопкой войти
const getSandbox_1 = code => ({
  files: {
    'src/index.js': defaultIndexJs,
    'src/index.test.js': {
      content:
        'import React from "react";\n' +
        '\n' +
        'import { configure, mount } from "enzyme";\n' +
        'import App from "./App";\n' +
        'import Adapter from "enzyme-adapter-react-16";\n' +
        'configure({ adapter: new Adapter() });\n' +
        '\n' +
        'test("renders learn react link", () => {\n' +
        '  const wrapper = mount(<App />);\n' +
        '  expect(wrapper.find({ type: "email" })).toHaveLength(1);\n' +
        '  expect(wrapper.find({ type: "password" })).toHaveLength(1);\n' +
        '  expect(wrapper.find({ type: "submit" })).toHaveLength(1);\n' +
        '});\n',
    },
    'src/App.js': {
      content:
        'import React from "react";\n' +
        '\n' +
        'export default function App() {\n' +
        '  return (\n' +
        '    <div className="App">\n' +
        code +
        '    </div>\n' +
        '  );\n' +
        '}\n',
    },
    'package.json': defaultPackageJson,
  },
});

// должен отправиться запрос например через fetch api - на любой адрес например google.com
const getSandbox_2 = code => ({
  files: {
    'src/index.js': defaultIndexJs,
    'src/App.js': {
      content:
        'import React from "react";\n' +
        '\n' +
        'export default function App() {\n' +
        code +
        '  return (\n' +
        '    <div className="App">\n' +
        'smth' +
        '    </div>\n' +
        '  );\n' +
        '}\n',
    },
    'src/index.test.js': { content: 'h' },
    'package.json': defaultPackageJson,
  },
});

// написать компонент App, который рендерит список фруктов (Apple, Banana, Orange)
const getSandbox_3 = code => ({
  files: {
    'src/index.js': defaultIndexJs,
    'src/index.test.js': {
      content:
        'import React from "react";\n' +
        '\n' +
        'import { configure, mount } from "enzyme";\n' +
        'import App from "./App";\n' +
        'import Adapter from "enzyme-adapter-react-16";\n' +
        'configure({ adapter: new Adapter() });\n' +
        '\n' +
        'test("renders list of fruits", () => {\n' +
        '  const wrapper = mount(<App />);\n' +
        '  expect(wrapper.find("ul").find("li")).toHaveLength(3);\n' +
        '  expect(wrapper.find("ul").text().includes("Apple")).toBe(true);\n' +
        '  expect(wrapper.find("ul").text().includes("Banana")).toBe(true);\n' +
        '  expect(wrapper.find("ul").text().includes("Orange")).toBe(true);\n' +
        '});\n',
    },
    'src/App.js': {
      content:
        'import React from "react";\n' + code + '\nexport default App;\n',
    },
    'package.json': defaultPackageJson,
  },
});

module.exports = { getSandbox_0, getSandbox_1, getSandbox_2, getSandbox_3 };
